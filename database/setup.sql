-------------------------------------------------------------------------------
-- USER
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS player;

CREATE TABLE player (
  id     VARCHAR(10) NOT NULL,
  points BIGINT      NOT NULL
);

ALTER TABLE player ADD CONSTRAINT pk_player_id PRIMARY KEY (id);

-------------------------------------------------------------------------------
-- TOURNAMENT
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS tournament;

CREATE TABLE tournament (
  id      BIGINT  NOT NULL,
  deposit BIGINT  NOT NULL,
  status  VARCHAR NOT NULL
);

ALTER TABLE tournament ADD CONSTRAINT pk_tournament_id PRIMARY KEY (id);

-------------------------------------------------------------------------------
-- PARTICIPANT
-------------------------------------------------------------------------------
DROP TABLE IF EXISTS participant;

CREATE SEQUENCE participant_id_seq INCREMENT 1 START 1;

CREATE TABLE participant (
  id            BIGINT      NOT NULL  DEFAULT NEXTVAL('participant_id_seq'),
  player_id     VARCHAR(10) NOT NULL,
  tournament_id BIGINT      NOT NULL,
  backer_id     VARCHAR(10) NULL,
  contribution  BIGINT      NOT NULL
);

ALTER TABLE participant ADD CONSTRAINT pk_participant_id PRIMARY KEY (id);

ALTER TABLE participant
  ADD CONSTRAINT fk_participant_player_id
FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE participant
  ADD CONSTRAINT fk_participant_backer_id
FOREIGN KEY (backer_id) REFERENCES player (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Do not allow multiple participation in tournament for player
CREATE UNIQUE INDEX idx_participant_player_id_tournament_id_backer_id ON participant (player_id, tournament_id, backer_id) WHERE backer_id IS NOT NULL;
CREATE UNIQUE INDEX idx_participant_player_id_tournament_id ON participant (player_id, tournament_id) WHERE backer_id IS NULL;
