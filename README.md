# Social tournament service #
-----
## Dependencies ##
* Golang v1.8+
* PostgreSQL 
* External Go packages dependencies

```bash
go get github.com/gorilla/mux           # HTTP router
go get github.com/lib/pq                # Postgres driver for database/sql package
```

## Running ##
	docker-compose up
	
Application is running on http://localhost:8080