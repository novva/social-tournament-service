package main

import (
	"bitbucket.org/novva/tournament/app"
	"log"
)

func main() {
	db, err := app.Connect()
	if err != nil {
		log.Fatal("Unable to open db connection")
	}
	router := app.NewRouter().InitRoutes()
	server := app.NewServer(db, router)
	server.Run(":8080")
}