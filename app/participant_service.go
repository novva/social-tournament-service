package app

import (
	"fmt"
	"database/sql"
	"strings"
)

const getParticipantsByPlayerIdsAndTournamentId = `SELECT * FROM participant WHERE player_id IN ($1) and tournament_id = ($2)`

func (tx *Tx) getParticipantsByPlayerIdsAndTournamentId(playerIds []string, tournamentId int) ([]*Participant, error) {
	rows, err := tx.Query(getParticipantsByPlayerIdsAndTournamentId,  strings.Join(playerIds,","), tournamentId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return mapParticipants(rows)
}

func (tx *Tx) requireParticipation(playerId string, tournamentId int) error {
	participants, err := tx.getParticipantsByPlayerIdsAndTournamentId([]string{playerId}, tournamentId)
	if err != nil {
		return err
	}
	if participants == nil {
		return BusinessError{fmt.Errorf("Player with id '%s' is not participating in tournament '%d'", playerId, tournamentId)}
	}
	return nil
}

const addParticipant = `INSERT INTO participant (player_id, tournament_id, backer_id, contribution)
						VALUES ($1, $2, $3, $4) RETURNING id`

func (tx *Tx) addParticipant(p *Participant) error {
	lastInsertId := ""
	if err := tx.QueryRow(addParticipant, p.PlayerId, p.TournamentId, p.BackerId, p.Contribution).Scan(&lastInsertId); err != nil {
		return err
	}
	return nil
}

const getBackerIds = `SELECT DISTINCT backer_id FROM participant WHERE player_id IN ($1) and tournament_id = ($2) AND backer_id IS NOT NULL`

func (tx *Tx) getBackerIds(playerIds []string, tournamentId int) ([]string, error) {
	rows, err := tx.Query(getBackerIds, strings.Join(playerIds,","), tournamentId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	beneficiaryIds := []string{}
	for rows.Next() {
		var beneficiaryId string
		rows.Scan(&beneficiaryId)
		beneficiaryIds = append(beneficiaryIds, beneficiaryId)
	}

	return beneficiaryIds, nil
}

func mapParticipants(rows *sql.Rows) ([]*Participant, error) {
	var participants []*Participant
	for rows.Next() {
		participant, err := scanParticipant(rows)
		if err != nil {
			return nil, fmt.Errorf("Could not read participant: %v", err)
		}
		participants = append(participants, participant)
	}
	return participants, nil
}
