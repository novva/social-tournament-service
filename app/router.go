package app

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Router struct {
	*mux.Router
}

func NewRouter() *Router {
	r := mux.NewRouter()
	return &Router{r}
}

type TournamentEnd struct {
	TournamentId int `json:"tournamentId,string,omitempty"`
	Winners      []Winner `json:"winners"`
}

type Winner struct {
	PlayerId string `json:"playerId,omitempty"`
	Prize    int `json:"prize,omitempty"`
}

func (r *Router) InitRoutes() *Router {
	r.Methods("GET")	.Path("/fund")					.Handler(ApiHandler(PlayerFundHandler))
	r.Methods("GET")	.Path("/take")					.Handler(ApiHandler(PlayerDefundHandler))
	r.Methods("GET")	.Path("/balance")				.Handler(ApiHandler(PlayerBalanceHandler))
	r.Methods("GET")	.Path("/announceTournament")	.Handler(ApiHandler(NewTournamentHandler))
	r.Methods("GET")	.Path("/joinTournament")		.Handler(ApiHandler(JoinTournamentHandler))
	r.Methods("GET")	.Path("/reset")					.Handler(ApiHandler(DatabaseResetHandler))
	r.Methods("POST")	.Path("/resultTournament")		.Handler(ApiHandler(EndTournamentHandler))

	return r
}

func DatabaseResetHandler(_ http.ResponseWriter, _ *http.Request) error {
	return appDb.reset()
}
