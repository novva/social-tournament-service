package app

import (
	"net/http"
	"strconv"
	"encoding/json"
	"fmt"
)

func PlayerFundHandler(_ http.ResponseWriter, r *http.Request) error {
	params, err := requireGetParams(r, "playerId", "points")
	if err != nil {
		return BusinessError{err}
	}
	points, err := strconv.Atoi(params["points"])
	if err != nil {
		return BusinessError{err}
	}
	id := params["playerId"]

	tx, err := appDb.Begin()
	tx.lockPlayers([]string{id})
	player, err := tx.getPlayer(id)
	if err != nil {
		return err
	}

	if err != nil {
		return nil
	}
	if player == nil {
		err := tx.addPlayer(&Player{Id: id, Points: points})
		if err != nil {
			return err
		}
	} else {
		err = tx.fundPlayer(id, points)
		if err != nil {
			return err
		}
	}
	tx.Commit()

	return nil
}

func PlayerDefundHandler(_ http.ResponseWriter, r *http.Request) error {
	params, err := requireGetParams(r, "playerId", "points")
	if err != nil {
		return err
	}
	points, err := strconv.Atoi(params["points"])
	if err != nil {
		return err
	}
	id := params["playerId"]

	tx, err := appDb.Begin()
	tx.lockPlayers([]string{id})
	if err != nil {
		return nil
	}
	err = tx.defundPlayer(id, points)
	if err != nil {
		return err
	}
	tx.Commit()

	return nil
}

func PlayerBalanceHandler(w http.ResponseWriter, r *http.Request) error {
	params, err := requireGetParams(r, "playerId")
	if err != nil {
		return err
	}
	id := params["playerId"]

	tx, err := appDb.Begin()
	tx.lockPlayers([]string{id})
	player, err := tx.getPlayer(id)
	if err != nil {
		return err
	}
	if player != nil {
		json.NewEncoder(w).Encode(player)
	} else {
		return BusinessError{fmt.Errorf("Could not find player with id '%s'", id)}
	}
	tx.Commit()

	return nil
}
