package app

// Helper method for calculation player/backer contribution/prize in a tournament
// e.g with 2 backers and tournament's deposit of 1000 points - each backer contributes 333 points and player 334
// or with 3 backers and prize of 1000 points - each backer (and winner) get 250 points
func ParticipationPoints(points int, numOfBackers int) (playerPoints int, bakerPoints int) {
	if numOfBackers == 0 {
		return points, 0
	}
	remainder := points % (numOfBackers + 1)
	bakerPoints = points / (numOfBackers + 1)
	if remainder == 0 {
		playerPoints = bakerPoints
	} else {
		playerPoints = bakerPoints + remainder
	}
	return playerPoints, bakerPoints
}
