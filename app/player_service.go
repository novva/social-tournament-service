package app

import (
	"fmt"
	"log"
	"database/sql"
	"strings"
)

const addPlayer = `INSERT INTO player (id, points) VALUES ($1, $2) RETURNING id`

func (tx *Tx) addPlayer(p *Player) error {
	lastInsertId := ""
	if err := tx.QueryRow(addPlayer, p.Id, p.Points).Scan(&lastInsertId); err != nil {
		return err
	}
	return nil
}

const getPlayer = `SELECT * FROM player WHERE id = ($1)`

func (tx *Tx) getPlayer(id string) (*Player, error) {
	player, err := scanPlayer(tx.QueryRow(getPlayer, id))
	if err != nil {
		if err == sql.ErrNoRows {
			log.Printf("Player with id '%s' does not exist", id)
			return nil, nil
		} else {
			return nil, fmt.Errorf("Could not get player: %v", err)
		}
	}
	return player, nil
}

func (tx *Tx) requirePlayer(id string) (*Player, error) {
	player, err := tx.getPlayer(id)
	if err != nil {
		return nil, fmt.Errorf("Could not get player: %v", err)
	} else {
		if player == nil {
			return nil, fmt.Errorf("Player with id '%s' not found", id)
		}
	}
	return player, nil
}

const fundPlayer = `UPDATE player SET points = points + ($1) WHERE id = ($2)`

func (tx *Tx) fundPlayer(id string, points int) error {
	_, err := tx.Exec(fundPlayer, points, id)
	if err != nil {
		return fmt.Errorf("Could not fund player: %v", err)
	}
	return nil
}

const defundPlayer = `UPDATE player SET points = points - ($1) WHERE id = ($2)`

func (tx *Tx) defundPlayer(id string, points int) error {
	player, err := tx.requirePlayer(id)
	if err != nil {
		return err
	}
	if player.Points < points {
		return BusinessError{fmt.Errorf("Insufficient funds for player with id '%s'", id)}
	}
	_, err = tx.Exec(defundPlayer, points, id)
	if err != nil {
		return fmt.Errorf("Could not defund player: %v", err)
	}
	return nil
}

const lockPlayers = `SELECT id FROM player WHERE id IN ($1) FOR UPDATE` // wait for

func (tx *Tx) lockPlayers(ids []string) error {
	_, err := tx.Exec(lockPlayers, strings.Join(ids,","))
	if err != nil {
		return err
	}

	return nil
}
