package app

import (
	"database/sql"
	"fmt"
)

const addTournament = `INSERT INTO tournament (id, deposit, status) VALUES ($1, $2, $3) RETURNING id`

func (db *DB) addTournament(t *Tournament) error {
	lastInsertId := ""
	if err := db.QueryRow(addTournament, t.Id, t.Deposit, t.Status).Scan(&lastInsertId); err != nil {
		return err
	}
	return nil
}

const getTournament = `SELECT * FROM tournament WHERE id = ($1)`

func (tx *Tx) RequireTournament(id int) (*Tournament, error) {
	tournament, err := scanTournament(tx.QueryRow(getTournament, id))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, BusinessError{fmt.Errorf("Tournament with id '%d' does not exist", id)}
		} else {
			return nil, fmt.Errorf("Could not get tournament: %v", err)
		}
	}
	return tournament, nil
}

const endTournament = `UPDATE tournament SET status = 'ENDED' WHERE id = ($1)`

func (tx *Tx) endTournament(id int) error {
	if _, err := tx.RequireTournament(id); err != nil {
		return nil
	}
	_, err := tx.Exec(endTournament, id)
	if err != nil {
		return fmt.Errorf("Could not end tournament: %v", err)
	}
	return nil
}