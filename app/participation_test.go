package app

import "testing"

func TestParticipationPointsNotEqual(t *testing.T) {
	pC, bC := ParticipationPoints(1000, 2)
	if pC != 334 {
		t.Errorf("Got %d as player's contribution, want %d", pC, 334)
	}

	if bC != 333 {
		t.Errorf("Got %d as backer's contribution, want %d", bC, 333)
	}
}

func TestParticipationPointsEqual(t *testing.T) {
	pC, bC := ParticipationPoints(1000, 3)
	if pC != 250 {
		t.Errorf("Got %d as player's contribution, want %d", pC, 250)
	}

	if bC != 250 {
		t.Errorf("Got %d as backer's contribution, want %d", bC, 250)
	}

}
