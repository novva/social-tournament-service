package app

import (
	"net/http"
	"strconv"
	"fmt"
	"encoding/json"
)

func NewTournamentHandler(_ http.ResponseWriter, r *http.Request) error {
	params, err := requireGetParams(r, "tournamentId", "deposit")
	if err != nil {
		return BusinessError{err}
	}
	deposit, err := strconv.Atoi(params["deposit"])
	if err != nil {
		return BusinessError{err}
	}

	id, err := strconv.Atoi(params["tournamentId"])
	if err != nil {
		return BusinessError{err}
	}

	tx, err := appDb.Begin()
	if err != nil {
		return nil
	}
	appDb.addTournament(&Tournament{Id: id, Deposit: deposit, Status: "ACTIVE"})
	tx.Commit()

	return nil
}

func JoinTournamentHandler(_ http.ResponseWriter, r *http.Request) error {
	params, err := requireGetParams(r, "tournamentId", "playerId")
	if err != nil {
		return BusinessError{err}
	}
	tournamentId, err := strconv.Atoi(params["tournamentId"])
	playerId := params["playerId"]
	if err != nil {
		return BusinessError{err}
	}
	backerIds := r.URL.Query()["backerId"]
	playerIds := append(backerIds, playerId)

	tx, err := appDb.Begin()
	tx.lockPlayers(playerIds)
	if err != nil {
		return nil
	}
	tournament, err := tx.RequireTournament(tournamentId)
	if err != nil {
		return err
	}
	playerContrPoints, backerContrPoints := ParticipationPoints(tournament.Deposit, len(backerIds))
	for _, backerId := range backerIds {
		err = tx.addParticipant(&Participant{PlayerId: playerId, TournamentId: tournamentId, BackerId: NullString(backerId), Contribution: backerContrPoints})
		if err != nil {
			return err
		}
		err = tx.defundPlayer(backerId, backerContrPoints)
		if err != nil {
			return err
		}
	}
	err = tx.addParticipant(&Participant{PlayerId: playerId, TournamentId: tournamentId, Contribution: playerContrPoints})
	if err != nil {
		return err
	}
	err = tx.defundPlayer(playerId, playerContrPoints)
	if err != nil {
		return err
	}
	tx.Commit()

	return nil
}

func EndTournamentHandler(_ http.ResponseWriter, r *http.Request) error {
	var e TournamentEnd
	if r.Body == nil {
		return BusinessError{fmt.Errorf("Request missing body")}
	}
	err := json.NewDecoder(r.Body).Decode(&e)
	if err != nil {
		return err
	}
	if len(e.Winners) == 0 {
		return BusinessError{fmt.Errorf("Winners are missing")}
	}

	tx, err := appDb.Begin()
	beneficiaryIds, err := findBeneficiaryIds(tx, e)
	if err != nil {
		return err
	}
	tx.lockPlayers(beneficiaryIds)
	if err != nil {
		return nil
	}

	for _, winner := range e.Winners {
		if err := tx.requireParticipation(winner.PlayerId, e.TournamentId); err != nil {
			return err
		}
		backerIds, err := tx.getBackerIds([]string{winner.PlayerId}, e.TournamentId)
		if err != nil {
			return err
		}
		playerPrizePoints, backerPrizeSum := ParticipationPoints(winner.Prize, len(backerIds))
		for _, backerId := range backerIds {
			if err = tx.fundPlayer(backerId, backerPrizeSum); err != nil {
				return err
			}
		}
		if err = tx.fundPlayer(winner.PlayerId, playerPrizePoints); err != nil {
			return err
		}
	}

	err = tx.endTournament(e.TournamentId)
	if err != nil {
		return err
	}
	tx.Commit()

	return nil
}

func findBeneficiaryIds(tx *Tx, e TournamentEnd) ([]string, error) {
	winnerIds := []string{}
	for _, winner := range e.Winners {
		winnerIds = append(winnerIds, winner.PlayerId)
	}
	beneficiaryIds, err := tx.getBackerIds(winnerIds, e.TournamentId)
	if err != nil {
		return nil, err
	}

	for _, winnerId := range winnerIds {
		if !contains(beneficiaryIds, winnerId) {
			beneficiaryIds = append(beneficiaryIds, winnerId)
		}
	}
	return beneficiaryIds, nil
}