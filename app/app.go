package app

import (
	"net/http"
)

var appDb *DB

type Server struct {
	db *DB
	router *Router
}

func NewServer(db *DB, r *Router) *Server {
	appDb = db
	s := &Server{db, r}
	return s
}

func (s *Server) Run(address string) *Server {
	http.ListenAndServe(address, s.router)
	return s
}