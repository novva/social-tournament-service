package app

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

const (
	HOST     = "postgres"
	PORT     = "5432"
	USER     = "tournament_app"
	PASSWORD = "3x1mpl3SwcV"
	DB_NAME  = "tournament_db"
)

type DB struct {
	*sql.DB
}

type rowScanner interface {
	Scan(target ...interface{}) error
}

// return database reference for a data source
func Connect() (*DB, error) {
	dataSource := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable", HOST, USER, PASSWORD, DB_NAME, PORT)
	db, _ := sql.Open("postgres", dataSource) // ignoring error since no actual connection is establish
	err := db.Ping()                          // here comes actual connection tryout
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

const reset = `
	DELETE FROM participant;
	DELETE FROM tournament;
	DELETE FROM player;
`

func (db *DB) reset() error {
	if _, err := db.Exec(reset); err != nil {
		return err
	}
	return nil
}

func NullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
