package app

import (
	"net/http"
	"fmt"
	"log"
)

type Error interface {
	error
	Status() int
}

type BusinessError struct {
	Err error
}

func (be BusinessError) Error() string {
	return be.Err.Error()
}

type ApiHandler func(http.ResponseWriter, *http.Request) error

func (fn ApiHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if e := fn(w, r); e != nil { // e is *appError, not os.Error.
		switch e := e.(type) {
		case BusinessError:
			http.Error(w, e.Error(), http.StatusBadRequest)
		default:
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		log.Printf("HTTP error: %s", e)
	}
}

func requireGetParams(r *http.Request, names ...string) (map[string]string, error) {
	var params = make(map[string]string)
	for _, name := range names {
		param := r.URL.Query().Get(name)
		if len(param) == 0 {
			return nil, fmt.Errorf("Required parameter '%s' not found", name)
		} else {
			params[name] = param
		}
	}

	return params, nil
}
