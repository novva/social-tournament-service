package app

import "database/sql"

type Player struct {
	Id     string   `json:"id"`
	Points int    	`json:"points"`
}

type Tournament struct {
	Id      int    	`json:"id"`
	Deposit int    	`json:"deposit"`
	Status  string  `json:"status"`
}

type Participant struct {
	Id           int        		`json:"id"`
	PlayerId     string     		`json:"playerId"`
	TournamentId int        		`json:"tournamentId"`
	BackerId     sql.NullString     `json:"backerId"`
	Contribution int        		`json:"contribution"`
}

func scanPlayer(s rowScanner) (*Player, error) {
	var (
		id     string
		points int
	)
	if err := s.Scan(&id, &points); err != nil {
		return nil, err
	}

	player := &Player{
		Id:     id,
		Points: points,
	}
	return player, nil
}

func scanTournament(s rowScanner) (*Tournament, error) {
	var (
		id      int
		deposit int
		status  string
	)
	if err := s.Scan(&id, &deposit, &status); err != nil {
		return nil, err
	}

	tournament := &Tournament{
		Id:      id,
		Deposit: deposit,
		Status:  status,
	}
	return tournament, nil
}

func scanParticipant(s rowScanner) (*Participant, error) {
	var (
		id           int
		playerId     string
		tournamentId int
		backerId     sql.NullString
		contribution int
	)
	if err := s.Scan(&id, &playerId, &tournamentId, &backerId, &contribution); err != nil {
		return nil, err
	}

	participant := &Participant{
		Id:           id,
		PlayerId:     playerId,
		TournamentId: tournamentId,
		BackerId:     backerId,
		Contribution: contribution,
	}
	return participant, nil
}
