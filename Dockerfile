FROM golang:latest
RUN go get github.com/gorilla/mux
RUN go get github.com/lib/pq

ADD . /go/src/bitbucket.org/novva/tournament
# Install binary globally within container
RUN go install bitbucket.org/novva/tournament

# Set binary as entrypoint
ENTRYPOINT /go/bin/tournament

EXPOSE 8080